---
title: Spring Boot 入门
---
# Spring Boot 入门

## Hibernate常用注解含义

### Hibernate的Annotation注解

1. 声明实体（Entity）

   1. @Entity：对实体注释。任何Hibernate映射对象都要有这个注释
   2. @Table：声明此对象映射到数据库的数据表，通过它可以为实体指定表(talbe),目录(Catalog)和schema的名字。该注释不是必须的，如果没有则系统使用默认值(实体的短类名)。
   3. @Version：该注释可用于在实体Bean中添加乐观锁支持。

2. 声明主键

   1. @Id：声明此属性为主键。该属性值可以通过应用自身创建，但是Hibernate推荐通过Hibernate生成
   2. @GeneratedValue：指定的

   

## 使用@Scheduled创建定时任务

[Spring Boot中使用@Scheduled创建定时任务](http://blog.didispace.com/springbootscheduled/)

## 将Spring Boot项目打包成jar包部署到Linux服务器持久运行

1. Linux服务器安装JDK环境[在CentOS7.4中安装jdk的几种方法及配置环境变量](https://blog.csdn.net/qq_32786873/article/details/78749384)

   1. 背景
      
      1. 因为Spring Boot中默认带有Tomcat，所以只需要在Linux服务器端配置好JDK即可直接运行，不需要在服务器再次安装Tomcat。
      
   2. 步骤
      1. 查看当前系统jdk的版本：java -version
      2. 安装需要的jdk版本的所有java程序：yum -y install java-1.8.0-openjdk*
      3. 查看java版本：java -version
      
   3. 通过idea将现有的spring boot项目打包成jar包
      1. idea打开项目
      2. 打开maven的管理器，通过maven里面的“Lifecycle”、“install”自动将现有的项目打包成jar包
      3. 在project的项目层级目录下的target文件夹下的jar包即为打包好的本项目jar包

   4. 将本地jar包上传到云端Linux服务器
      1. 下载一个Xftp工具（官网有教育版可免费使用）
      2. 通过Xftp连接服务器端，将打包好的jar包上传到服务器

   5. 持久运行jar包
      1. 背景知识：运行时，如果仅仅使用java -jar xxx.jar运行，那么当控制台关闭后，jar包就会停止运行，所以我们需要后台持久运行jar包，保证任何时候都可以使用jar包提供的功能。

      2. 一些简单的Linux操作命令

         1. 执行jar包的命令和在windows操作系统上是一样的，都是java -jar xxxx.jar。

         2. 将jar程序设置成后台运行，并且将标准输出的日志重定向至文件consoleMsg.log。

            nohup java -jar getCimiss-surf.jar >consoleMsg.log 2>&1 &

         3. 如果想杀掉运行中的jar程序，查看进程命令为：

            ps aux|grep getCimiss-surf.jar

            将会看到此jar的进程信息

            data 5796 0.0 0.0 112656 996 pts/1 S+ 09:11 0:00 grep --color=auto getCimiss-surf.jar data 30768 6.3 0.4 35468508 576800 ? Sl 09:09 0:08 java -jar getCimiss-surf.jar

            其中30768则为此jar的pid，杀掉命令为

            kill -9 30768

   6. 开放服务器的安全组端口

      1. 假设当前jar包的端口为9010，则需要在安全组中开放9010的端口

   7. 访问swagger

      1. 访问格式：服务器ip:启动端口/swagger-ui.html
      2. 使用http://xxx.xxx.xxx.xxx:9010/swagger-ui.html访问服务

   


# 参考资料

1. [SPRING中常用的注解（@ENTITY，@TABLE，@COLUMN，@REPOSITORY，@SERVICE）](https://www.cnblogs.com/hoojjack/p/6568920.html)
2. [Spring Boot中使用@Scheduled创建定时任务](http://blog.didispace.com/springbootscheduled/)
3. [Spring Boot教程](http://blog.didispace.com/categories/Spring-Boot/page/7/)
4. [在CentOS7.4中安装jdk的几种方法及配置环境变量](https://blog.csdn.net/qq_32786873/article/details/78749384)
5. [spring boot 项目部署到服务器 两种方式](https://blog.csdn.net/qq_22638399/article/details/81506448)
6. [linux jar 包运行与关闭](https://blog.csdn.net/qq_39507276/article/details/82227416)
7. [解决linux环境下nohup: redirecting stderr to stdout问题](https://blog.csdn.net/educast/article/details/28273301)